
const minMax = (val, min, max) => {
  return Math.min(Math.max(min, val), max);
}


export default {
  minMax,
}
