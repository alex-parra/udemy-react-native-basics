import React, { useReducer } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import utils from '../utils';

import ColorTinterTone from '../components/ColorTinterTone';

const COLOR_STEP = 10;
const COLOR_MAX = 255;
const COLOR_MIN = 0;

const colorReducer = (state, action) => {
  if (!state[action.color]) return { ...state };

  let newColorValue = state[action.color];
  if (action.task === 'increment') newColorValue += COLOR_STEP;
  if (action.task === 'decrement') newColorValue -= COLOR_STEP;

  return { ...state, [action.color]: utils.minMax(newColorValue, COLOR_MIN, COLOR_MAX) };
};

const ColorTinterScreen = ({ navigation }) => {
  const [state, dispatch] = useReducer(colorReducer, { red: 125, green: 125, blue: 125 });

  const { red, green, blue } = state;

  return (
    <View>
      <View style={{ ...styles.swatch, backgroundColor: `rgb(${red}, ${green}, ${blue})` }}>
        <Text style={{ textAlign: 'center' }}>
          {red}, {green}, {blue}
        </Text>
      </View>

      <ColorTinterTone color="red" value={red} onPress={(color, task) => dispatch({ color, task })} />
      <ColorTinterTone color="green" value={green} onPress={(color, task) => dispatch({ color, task })} />
      <ColorTinterTone color="blue" value={blue} onPress={(color, task) => dispatch({ color, task })} />
    </View>
  );
};

const styles = StyleSheet.create({
  swatch: {
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ColorTinterScreen;
