import React, { useState } from 'react';
import { View, Text, Button, FlatList } from 'react-native';
import ColorTiles from '../components/ColorTiles';
import NextScreen from '../components/NextScreen';

const RandomColorsScreen = ({navigation}) => {
  const [colors, setColors] = useState([]);

  const randomColorValue = () => {
    return Math.floor(256 * Math.random()).toFixed(0);
  };

  const newColor = () => {
    const rgb = [randomColorValue(), randomColorValue(), randomColorValue()];
    setColors([...colors, { rgb }]);
  };

  return (
    <View>
      <Button title="New Color" onPress={() => newColor()} />
      <ColorTiles colors={colors} />

      <NextScreen onPress={() => navigation.navigate('ColorTinter')} />
    </View>
  );
};

export default RandomColorsScreen;
