import React, { useReducer } from 'react';
import { Text, StyleSheet, View, Button } from 'react-native';
import NextScreen from '../components/NextScreen';

const counterReducer = (state, action) => {
  const amount = action.payload || 1;
  let newCount = state.counter;
  if (action.type === 'increment') newCount += amount;
  if (action.type === 'decrement') newCount -= amount;
  return { ...state, counter: newCount };
};

const HomeScreen = ({ navigation }) => {
  const [state, dispatch] = useReducer(counterReducer, { counter: 0 });

  return (
    <View>
      <Text style={styles.counter}>Count: {state.counter}</Text>
      <View style={styles.counterActions}>
        <Button title="+ Increment" onPress={() => dispatch({ type: 'increment' })} />
        <Button title="- Decrement" onPress={() => dispatch({ type: 'decrement' })} />
      </View>

      <NextScreen onPress={() => navigation.navigate('RandomColors')} />
    </View>
  );
};

const styles = StyleSheet.create({
  counter: {
    fontSize: 30,
    marginVertical: 40,
    textAlign: 'center',
  },
  counterActions: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HomeScreen;
