import React from 'react';
import { StyleSheet, View, Button } from 'react-native';

const NextScreen = ({onPress}) => {
  return (
    <View style={styles.nextWrap}>
      <Button title="Next >" onPress={() => onPress()} />
    </View>
  );
}

const styles = StyleSheet.create({
  nextWrap: {
    marginTop: 80,
    paddingTop: 20,
    borderTopColor: '#999',
    borderTopWidth: 1,
  }
});

export default NextScreen;
