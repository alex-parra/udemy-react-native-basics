import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const ColorTile = props => {
  const { color } = props;
  return (
    <View style={{ ...styles.tile, backgroundColor: `rgb(${color.rgb[0]}, ${color.rgb[1]}, ${color.rgb[2]})` }}>
      <Text style={styles.tileTxt}>{color.rgb.join('\n')}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  tile: {
    width: '25%',
    aspectRatio: 1,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 0,
    flexShrink: 1,
  },
  tileTxt: {
    fontSize: 14,
    textAlign: 'center',
  },
});

export default ColorTile;
