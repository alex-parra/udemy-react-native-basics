import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

const ColorTinterTone = ({color, value, onPress}) => {
  return (
    <View style={styles.wrap}>
      <Text style={styles.label}>{color.substring(0, 1).toUpperCase()}{color.substring(1)}:</Text>
      <View style={styles.button}><Button title="-" onPress={() => onPress(color, 'decrement')} /></View>
      <Text>{value}</Text>
      <View style={styles.button}><Button title="+" onPress={() => onPress(color, 'increment')} /></View>
    </View>
  );
}

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontSize: 20,
    fontWeight: 'bold',
    flex: 0,
    width: '30%',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  button: {
    flex: 1,
  },
});

export default ColorTinterTone;
