import React from 'react';
import { StyleSheet, FlatList, Text } from 'react-native';

import ColorTile from './ColorTile';

const ColorTiles = props => {
  return (
    <FlatList
      data={props.colors}
      renderItem={({ item }) => <ColorTile color={item} />}
      keyExtractor={item => item.rgb.join('')}
      numColumns={4}
    />
  );
};

export default ColorTiles;
