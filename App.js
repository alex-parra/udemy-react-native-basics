import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/screens/HomeScreen';
import RandomColorsScreen from './src/screens/RandomColorsScreen';
import ColorTinterScreen from './src/screens/ColorTinterScreen';

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    RandomColors: RandomColorsScreen,
    ColorTinter: ColorTinterScreen,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'App'
    }
  }
);

export default createAppContainer(navigator);
